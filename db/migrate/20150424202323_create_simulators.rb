class CreateSimulators < ActiveRecord::Migration
  def change
    create_table :simulators do |t|
      t.int :id
      t.string :name
      t.string :url

      t.timestamps null: false
    end
  end
end
