json.array!(@simulators) do |simulator|
  json.extract! simulator, :id, :id, :name, :url
  json.url simulator_url(simulator, format: :json)
end
