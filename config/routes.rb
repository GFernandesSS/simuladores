Rails.application.routes.draw do
  resources :simulators

  resources :subjects

  # You can have the root of your site routed with "root"
  root 'application#hello'
end